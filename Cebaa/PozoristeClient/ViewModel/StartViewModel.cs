﻿using PozoristeClient.Helpers;
using Pozoristeee.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PozoristeClient.ViewModel
{
    public class StartViewModel : ViewBase
    {
        public Window Window { get; set; }
        UnitOfWork unitOfWork = new UnitOfWork(new Pozoristeee.Model1Container3());

        public CommandBase RegistracijaCommand { get; set; }
        public CommandBase LoginCommand { get; set; }

        public StartViewModel()
        {
            RegistracijaCommand = new CommandBase(OnViewRegistracija);
            //LoginCommand = new CommandBase();
        }

        public void OnViewRegistracija(object parameter)
        {
            new RegistracijaWindow().ShowDialog();
        }
    }
}
